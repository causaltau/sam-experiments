# Structural Agnostic Model Experiments

## Authors

- SAM was initially developed by Diviyan Kalainathan and Olivier Goudet.

## Requirements

- Python 3.9+
- Structural Agnostic Model (https://gitlab.inria.fr/causaltau/sam)

```bash
pip install git+gitlab.inria.fr/causaltau/sam
```

## Citation

```latex
@article{kalainathan:22,  
  author    = {Kalainathan, Diviyan and Goudet, Olivier and Guyon, Isabelle and Lopez-Paz, David and Sebag, Mich{\`e}le},
  title     = {Structural Agnostic Modeling: Adversarial Learning of Causal Graphs},
  journal   = {Journal of Machine Learning Research},
  volume    = {23},
  number    = {219},
  pages     = {1--62},
  year      = {2022}
}
```

## License

Copyright (c) 2018-2023 the original author or authors.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the MIT License (MIT).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.